import 'package:app_shopping/components/common/component_notification.dart';
import 'package:app_shopping/pages/page_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<int?> getMemberId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('memberId');
  }

  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }

  static void setMemberId(int memberId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('memberId', memberId);
  }

  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();  // api불러오는동안 다른짓하는것을 방지하기위해

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();  // 메모리를 깨긋하게 비움 다 삭제한다는 뜻.

    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그인 페이지로 이동합니다.',
    ).call();

    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
  }
}