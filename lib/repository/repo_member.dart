import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/login_request.dart';
import 'package:app_shopping/model/login_result.dart';
import 'package:dio/dio.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUrl/member/login';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: loginRequest.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return LoginResult.fromJson(response.data);
  }
}