import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/banner_response.dart';
import 'package:dio/dio.dart';

class RepoBanner {
  Future<BannerResponse> getList() async {
    const String baseUrl = '$apiUrl/banner/list';

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return BannerResponse.fromJson(response.data);
  }
}