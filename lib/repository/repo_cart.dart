import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/cart_request.dart';
import 'package:app_shopping/model/cart_response.dart';
import 'package:app_shopping/model/common_response.dart';
import 'package:dio/dio.dart';

class RepoCart {
  Future<CommonResponse> setData(int goodsId) async {
    const String baseUrl = '$apiUrl/my-cart/new';

    CartRequest request = CartRequest(1, goodsId);

    Dio dio = Dio();

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResponse.fromJson(response.data);
  }

  Future<CartResponse> getList() async {
    const String baseUrl = '$apiUrl/my-cart/list/member-id/1';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CartResponse.fromJson(response.data);
  }

  Future<CommonResponse> delData(int id) async {
    const String baseUrl = '$apiUrl/my-cart/goods/cart-id/{id}';

    Dio dio = Dio();

    final response = await dio.delete(
        baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResponse.fromJson(response.data);
  }

}