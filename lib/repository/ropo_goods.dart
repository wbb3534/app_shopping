import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/goods_response.dart';
import 'package:dio/dio.dart';

class RepoGoods {
  Future<GoodsResponse> getList(String category) async {
    const String baseUrl = '$apiUrl/goods/list';

    Map<String, dynamic> params = {};
    params['category'] = category;

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        queryParameters: params,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return GoodsResponse.fromJson(response.data);
  }
}
