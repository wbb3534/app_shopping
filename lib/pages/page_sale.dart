import 'package:app_shopping/components/common/component_custom_loading.dart';
import 'package:app_shopping/components/common/component_notification.dart';
import 'package:app_shopping/components/component_img_text_card_item.dart';
import 'package:app_shopping/components/component_title_hr.dart';
import 'package:app_shopping/model/goods_item.dart';
import 'package:app_shopping/repository/ropo_goods.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PageSale extends StatefulWidget {
  const PageSale({Key? key}) : super(key: key);

  @override
  State<PageSale> createState() => _PageSaleState();
}

class _PageSaleState extends State<PageSale> {

  List<GoodsItem> goodsListFood = [];

  Future<void> _getGoodsList(String category) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGoods().getList(category).then((res) {
      setState(() {
        goodsListFood = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _getGoodsList('FOOD');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('쇼핑몰')),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const ComponentTitleHr(title: '식품'),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for (int i = 0; i < goodsListFood.length; i++)
                    ComponentImgTextCardItem(
                        goodsImageUrl: goodsListFood[i].goodsImageUrl,
                        goodsName: goodsListFood[i].goodsName,
                        goodsPrice: goodsListFood[i].goodsPrice,
                        salePrice: goodsListFood[i].salePrice,
                        saleResultPrice: goodsListFood[i].saleResultPrice,
                        voidCallback: () {},
                    )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  CarouselOptions _mainSliderOption() {
    return CarouselOptions(
      height: 300,
      aspectRatio: 16 / 9,
      viewportFraction: 0.8,
      initialPage: 0,
      enableInfiniteScroll: true,
      reverse: false,
      autoPlay: true,
      autoPlayInterval: Duration(seconds: 3),
      autoPlayAnimationDuration: Duration(milliseconds: 800),
      autoPlayCurve: Curves.fastOutSlowIn,
      enlargeCenterPage: true,
      enlargeFactor: 0.3,
      scrollDirection: Axis.horizontal,
    );
  }
}
