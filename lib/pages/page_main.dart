import 'package:app_shopping/components/common/component_custom_loading.dart';
import 'package:app_shopping/components/common/component_notification.dart';
import 'package:app_shopping/components/component_banner.dart';
import 'package:app_shopping/components/component_img_text_card_item.dart';
import 'package:app_shopping/components/component_title_hr.dart';
import 'package:app_shopping/model/banner_item.dart';
import 'package:app_shopping/model/goods_item.dart';
import 'package:app_shopping/repository/repo_banner.dart';
import 'package:app_shopping/repository/repo_cart.dart';
import 'package:app_shopping/repository/ropo_goods.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PageMain extends StatefulWidget {
  const PageMain({Key? key}) : super(key: key);

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {

  List<BannerItem> bannerList = [];

  List<GoodsItem> goodsListFood = [];

  Future<void> _getBannerList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBanner().getList().then((res) {
      setState(() {
        bannerList = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  Future<void> _getGoodsList(String category) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGoods().getList(category).then((res) {
      setState(() {
        goodsListFood = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  Future<void> _setCart(int goodsId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoCart().setData(goodsId).then((res) {
      ComponentNotification(
        success: true,
        title: '장바구니 추가 완료',
        subTitle: '장바구니 추가 완료 되었습니다.',
      ).call();

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '장바구니 추가 실패',
        subTitle: '장바구니 추가에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }


  @override
  void initState() {
    super.initState();
    _getGoodsList('FOOD');
    _getBannerList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('쇼핑몰')),
        body: SingleChildScrollView(
          child: Column(
            children: [
              CarouselSlider(
                options: _mainSliderOption(),
                items: bannerList.map((item) => ComponentBanner(imageUrl: item.bannerImageUrl)).toList(),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const ComponentTitleHr(title: '식품'),
                  SizedBox(
                    height: 10,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                          for (int i = 0; i < goodsListFood.length; i++)
                            ComponentImgTextCardItem(
                                goodsImageUrl: goodsListFood[i].goodsImageUrl,
                                goodsName: goodsListFood[i].goodsName,
                                goodsPrice: goodsListFood[i].goodsPrice,
                                salePrice: goodsListFood[i].salePrice,
                                saleResultPrice: goodsListFood[i].saleResultPrice,
                              voidCallback: () {
                                _setCart(goodsListFood[i].id);
                              },
                            )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
    );
  }

  CarouselOptions _mainSliderOption() {
    return CarouselOptions(
      height: 300,
      aspectRatio: 16 / 9,
      viewportFraction: 0.8,
      initialPage: 0,
      enableInfiniteScroll: true,
      reverse: false,
      autoPlay: true,
      autoPlayInterval: Duration(seconds: 3),
      autoPlayAnimationDuration: Duration(milliseconds: 800),
      autoPlayCurve: Curves.fastOutSlowIn,
      enlargeCenterPage: true,
      enlargeFactor: 0.3,
      scrollDirection: Axis.horizontal,
    );
  }
}
