import 'package:app_shopping/functions/token_lib.dart';
import 'package:flutter/material.dart';

class PageMy extends StatefulWidget {
  const PageMy({Key? key}) : super(key: key);

  @override
  State<PageMy> createState() => _PageMyState();
}

class _PageMyState extends State<PageMy> {

  String? memberName;

  Future<void> getMemberName() async {
    String? result = await TokenLib.getMemberName();
    setState(() {
      memberName = result;
    });
  }

  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }

  void initState() {
    super.initState();
    getMemberName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 2,
            child: Image.asset('assets/main_banner.jpg', fit: BoxFit.fill),
          ),
          Container(
            color: Colors.grey,
            height: MediaQuery.of(context).size.height / 10,
            child: Center(
              child: Text(
                '$memberName님 즐거운 시간을 즐기고 계시나요?',
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          OutlinedButton(
            onPressed: () {
              _asyncConfirmDialog(context);
            },
            style: ElevatedButton.styleFrom(
              primary: Colors.black,
              onPrimary: Colors.white
            ),
            child: const Text('로그아웃'),
          ),
        ],
      ),
    );
  }

  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('로그아웃'),
          content: Text('정말 로그아웃 하시겠습니까?'),
          actions: <Widget>[
            TextButton(
              child: Text('취소'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('확인'),
              onPressed: () {
                _logout(context);
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
}
