class CartRequest {
  int goodsId;
  int memberId;

  CartRequest(this.goodsId, this.memberId);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = Map<String, dynamic>();

    result['goodsId'] = this.goodsId;
    result['memberId'] = this.memberId;

    return result;
  }
}