class CartItem {
  int id;
  int memberId;
  int goodsId;
  String category;
  String goodsImageUrl;
  String goodsName;
  double goodsPrice;
  double salePrice;
  double saleResultPrice;

  CartItem(this.id, this.memberId, this.goodsId, this.category, this.goodsImageUrl, this.goodsName, this.goodsPrice, this.salePrice, this.saleResultPrice);

  factory CartItem.fromJson(Map<String, dynamic> json) {
    return CartItem(
      json['id'],
      json['memberId'],
      json['goodsId'],
      json['category'],
      json['goodsImageUrl'],
      json['goodsName'],
      json['goodsPrice'],
      json['salePrice'],
      json['saleResultPrice'],
    );
  }
}