import 'package:app_shopping/model/goods_item.dart';

class GoodsResponse {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<GoodsItem> list;

  GoodsResponse(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory GoodsResponse.fromJson(Map<String, dynamic> json) {
    return GoodsResponse(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => GoodsItem.fromJson(e)).toList()
    );
  }

}