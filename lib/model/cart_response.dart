

import 'package:app_shopping/model/cart_item.dart';

class CartResponse {
  bool isSuccess;
  int code;
  String msg;
  List<CartItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;

  CartResponse(this.isSuccess, this.code, this.msg, this.list, this.totalItemCount, this.totalPage, this.currentPage);

  factory CartResponse.fromJson(Map<String, dynamic> json) {
    return CartResponse(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => CartItem.fromJson(e)).toList(),
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage']
    );
  }
}