import 'package:app_shopping/model/banner_item.dart';

class BannerResponse {
  bool isSuccess;
  int code;
  String msg;
  List<BannerItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;

  BannerResponse(this.isSuccess, this.code, this.msg, this.list, this.totalItemCount, this.totalPage, this.currentPage);

  factory BannerResponse.fromJson(Map<String, dynamic> json) {
    return BannerResponse(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => BannerItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage']
    );
  }

}