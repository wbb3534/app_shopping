class BannerItem {
  int id;
  String bannerImageUrl;
  bool isUse;

  BannerItem(this.id, this.bannerImageUrl, this.isUse);

  factory BannerItem.fromJson(Map<String, dynamic> json) {
    return BannerItem(
      json['id'],
      json['bannerImageUrl'],
      json['isUse'] as bool,
    );
  }
}