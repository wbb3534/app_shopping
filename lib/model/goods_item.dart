class GoodsItem {
  int id;
  String category;
  String goodsImageUrl;
  String goodsName;
  double goodsPrice;
  double salePrice;
  double saleResultPrice;

  GoodsItem(this.id, this.category, this.goodsImageUrl, this.goodsName, this.goodsPrice, this.salePrice, this.saleResultPrice);

  factory GoodsItem.fromJson(Map<String, dynamic> json) {
    return GoodsItem(
        json['id'],
        json['category'],
        json['goodsImageUrl'],
        json['goodsName'],
        json['goodsPrice'],
        json['salePrice'],
        json['saleResultPrice']);
  }
}