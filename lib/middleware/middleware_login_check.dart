import 'package:app_shopping/functions/token_lib.dart';
import 'package:app_shopping/pages/page_index.dart';
import 'package:app_shopping/pages/page_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    int? memberId = await TokenLib.getMemberId();

    // 만약 멤버 id가 널값이면 PageLogin페이지로 보내고 아이디가 있으면 PageIndex페이지로 보낸다.
    if (memberId == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
    }
  }
}