import 'package:app_shopping/model/cart_item.dart';
import 'package:flutter/material.dart';

class ComponentCartItem extends StatelessWidget {
  const ComponentCartItem({super.key, required this.cartItem, required this.voidCallback});

  final CartItem cartItem;
  final VoidCallback voidCallback;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.5,
          color: Colors.black,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 200,
            height: 200,
            child: Image.network(cartItem.goodsImageUrl,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                cartItem.goodsName,
              ),
              Text(
                '정가: ${cartItem.goodsPrice}',
              ),
              Text(
                '할인금액: ${cartItem.salePrice}',
              ),
              Text(
                '총 금액: ${cartItem.saleResultPrice}',
              ),
            ],
          ),
          SizedBox(
            width: 20,
          ),
          OutlinedButton(
            onPressed: voidCallback,
            style: ElevatedButton.styleFrom(
                primary: Colors.red, onPrimary: Colors.white),
            child: const Text('삭제'),
          ),
        ],
      ),
    );
  }
}
