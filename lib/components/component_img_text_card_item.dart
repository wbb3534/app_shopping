import 'package:flutter/material.dart';

class ComponentImgTextCardItem extends StatelessWidget {
  const ComponentImgTextCardItem({super.key, required this.goodsImageUrl, required this.goodsName, required this.goodsPrice, required this.salePrice, required this.saleResultPrice, required this.voidCallback});

  final String goodsImageUrl;
  final String goodsName;
  final double goodsPrice;
  final double salePrice;
  final double saleResultPrice;
  final VoidCallback voidCallback;


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
        ),
        border: Border.all(
          width: 1,
          color: Colors.black,
        ),
      ),
      child: Column(
        children: [
          SizedBox(
            width: 200,
            height: 150,
            child: Image.network('${goodsImageUrl}', fit: BoxFit.fill,),
          ),
          Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Text('상품명: ${goodsName}',
                style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    height: 2
                ),
              ),
              Text('정가: ${goodsPrice}',
                style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey,
                    height: 2
                ),
              ),
              Text('할인금액: ${salePrice}',
                style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                  color: Colors.red,
                ),),
              Text('총 금액: ${saleResultPrice}',
                style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    height: 2
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              OutlinedButton(
                  onPressed: voidCallback,
                  child: const Text('담기')
              ),
            ],
          ),
        ],
      ),
    );
  }
}
