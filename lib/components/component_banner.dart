import 'package:flutter/material.dart';

class ComponentBanner extends StatelessWidget {
  const ComponentBanner({super.key, required this.imageUrl});

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Image.network(imageUrl,
              fit: BoxFit.cover, width: 1000)),
    );
  }
}
