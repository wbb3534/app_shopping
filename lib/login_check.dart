import 'package:app_shopping/middleware/middleware_login_check.dart';
import 'package:flutter/material.dart';

// 로그인시 PageIndex 로그인 안 할시 pageLogin페이지로 가게하는 페이지이다.
// 구분하는법은 메모리에 id랑 username 이 있냐 없냐로 구별
class LoginCheck extends StatefulWidget {
  const LoginCheck({Key? key}) : super(key: key);

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {

  @override
  void initState() {
    super.initState();
    MiddlewareLoginCheck().check(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(),
    );
  }
}
